<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class Tarifa extends Model 
{

    public function get_infoTarifaTipoVeh($tipoVehiculo){
        return $this->infoTarifaTipoVeh($tipoVehiculo);
    }
    
    public function set_regNewTarifa($request){
        return $this->regNewTarifa($request);
    }

    public function set_updateTarifa($request){
        return $this->updateTarifa($request);
    }

    private function infoTarifaTipoVeh($tipoVehiculo){
        $infoTarifaVehiculo = DB::table('tarifas')
                            ->select('tipoVehiculo', 'valorMinuto')
                            ->where('tipoVehiculo', '=', $tipoVehiculo)
                            ->get();
        return $infoTarifaVehiculo;

    }

    private function regNewTarifa($request){
        $fechaActual=Carbon::now();
        DB::table('tarifas')
        ->insert([
            "tipoVehiculo"=>$request->tipoVehiculo,
            "valorMinuto"=>$request->valorMinuto,
            "created_at"=>$fechaActual
        ]);

        return "Reg_Exitoso";
    }

    private function updateTarifa($request){
        $fechaActual=Carbon::now();
        DB::table('tarifas')
        ->where('tipoVehiculo', '=', $request->tipoVehiculo)
        ->update(['valorMinuto'=>$request->valorMinuto, 'updated_at'=>$fechaActual]);
        return "Reg_Actualizado";
    }
}
