<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class Promocion extends Model 
{
    protected $table="promociones";

    public function set_regNewPromocion($request){
        return $this->regNewPromocion($request);
    }

    public function set_updatePromocion($request){
        return $this->updatePromocion($request);
    }

    private function regNewPromocion($request){
        $fechaActual=Carbon::now();
        DB::table('promociones')
        ->insert([
            "nombre_promocion"=>$request->nombrePromocion,
            "descuento"=>$request->descuento,
            "minutos"=>$request->minutosPromocion,
            "created_at"=>$fechaActual,
        ]);

        return "Reg Exitoso";
    }

    private function updatePromocion($request){
        $fechaActual=Carbon::now();
        DB::table('promociones')
        ->update(['nombre_promocion'=>$request->nombrePromocion, 'descuento'=>$request->descuento,
        'minutos'=>$request->minutosPromocion, 'updated_at'=>$fechaActual
        ]);
        return "Reg Exitoso";
    }

}