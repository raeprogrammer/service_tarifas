<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Promocion;
class PromocionesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function regNewPromocion(Request $request){
        $promocion = new Promocion;
        /* Se verifica si existen promociones registradas */
        $promociones=$promocion->all();
        if(count($promociones)>0){
            $update=$promocion->set_updatePromocion($request);
        }else{
            $regPromocion=$promocion->set_regNewPromocion($request);
        }
        return response()->json([
            'estado_operacion'=>"Exitosa",
            'data'=>"Promoción Registrada"
        ]);  
        
    }

    public function listPromocion(){
        $promocion = new Promocion;
        $promociones=$promocion->all();
        return response()->json([
            'estado_operacion'=>"Exitosa",
            'data'=>$promociones
        ]);  
    }
}