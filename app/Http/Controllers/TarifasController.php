<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Tarifa;
class TarifasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function regNewTarifa(Request $request){
        $tarifa = new Tarifa;
        /* Se consulta si existe tarifa registrada para el tipo de vehiculo */
        $tarifaReg=$tarifa->get_infoTarifaTipoVeh($request->tipoVehiculo);
        if(count($tarifaReg)>0){
            /* Tarifa registrada se procede a actualizar */
            $updateTarifa=$tarifa->set_updateTarifa($request);
        }else{
            $regTarifa=$tarifa->set_regNewTarifa($request);
        }

        return response()->json([
            'estado_operacion'=>"Exitosa",
            'data'=>"Tarifa Registrada"
        ]);  

    }

    public function listTarifas(){
        $tarifa = new Tarifa;
        $listTarifas=$tarifa->all();
        return response()->json([
            'estado_operacion'=>"Exitosa",
            'data'=>$listTarifas
        ]); 
    }
}