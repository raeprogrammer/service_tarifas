<?php

namespace App\Http\Middleware;
use Illuminate\Http\Response;
use Closure;

class AuthAccessApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $validSecrets=explode(',', env('ACCEPTED_SECRETS'));
        if(in_array($request->header('Autorization'), $validSecrets)){
            return $next($request);
        }
      //  return $next($request);
        abort(Response::HTTP_UNAUTHORIZED);
      
    }
}
